# Encore hasznalat

## Telepites
Szukseges hozza Node program es az NPM es Yarn csomagkezelo.

Elso lepesben a NVM programot tedd fel:

https://github.com/nvm-sh/nvm#installing-and-updating

Majd `nvm install 13 && nvm use 13`

Ez utan mehet a yarn telepites
https://classic.yarnpkg.com/en/docs/install/#mac-stable

### Ajanlott verziok
```
gonzoo@gonzoo-imac-i9 ● nvm current
v13.11.0
gonzoo@gonzoo-imac-i9 ● node -v
v13.11.0
gonzoo@gonzoo-imac-i9 ● npm -v
6.13.7
gonzoo@gonzoo-imac-i9 ● yarn -v
1.22.1
```

---

A mukodeshez szukseges alap csomagok telepitesehez a `yarn install` parancsot kell hasznalni.

Az alap csomag mar tartalmazza az alabbiakat is:
- Bootstrap 4.4.1
- jQuery 3.4.1
 
# Konyvtarak
Minden assetet az `./assets` konyvtarban kell elhelyezni

Az Encore belepis pontja az `./assets/app.js`, itt kell importalni minden hasznalatos csomagot.


# Futtatas

`yarn dev-server`